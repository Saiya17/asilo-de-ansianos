/*¿Cuáles son los ancianos que sufren de alergias al Polen?*/

Select NOMBRE, ALERGIAS from ANCIANO
where ALERGIAS = 'Polen'

/*¿Conocer el nombre del trabajador, el lugar de trabajo, el cargo que ocupa y la fecha de entrada de todos los empleados?*/

Select NOMBRET AS EMPLEADO,
NOMBRE AS LUGAR_DE_TRABAJO,
CARGO,
FECHA_DE_ENTRADA
FROM EMPLEADO
INNER JOIN ASILO ON ASILO.CODE_ASILO = EMPLEADO.CODE_ASILO

/*¿Conocer el nombre del anciano, Que enfermedad sufre y que sean solo de la ciudad de manta?*/

SELECT
NOMBRE,
ENFERMEDADES,
CIUDAD
FROM ANCIANO
WHERE CIUDAD = 'Manta'


/*¿Mostrar la información del asilo y su menú de comida solamente de los días Sábado?*/

Select *
FROM MENU_COMIDA
INNER JOIN ASILO ON ASILO.CODE_ASILO = MENU_COMIDA.CODE_ASILO
WHERE DIA = 'Sabado'