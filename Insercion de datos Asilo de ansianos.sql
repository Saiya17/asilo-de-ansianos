/*
Insertar registros a nuestra tabla ANCIANO
*/
insert into ANCIANO values
(1,'1','Polen','1310767025','Manta','Calle 10 Avenida 15','Cardio Vascular','Soltero','1960-05-11','2','Carlos Javier','Ecuador','Manabi','36','38','2876654','2017-03-25','','Ninguna'),
(2,'1','Polvo','1310767035','Portoviejo','Calle 4 Avenida 6','Cancer de mama','Soltera','1965-08-15','7','Ana Gabriela','Ecuador','Manabi','30','32','2345987','2014-09-13','','Ninguna'),
(3,'1','Mascotas','1310767045','Santa Ana','Calle 25 Avenida 10','Lepra','Soltera','1970-07-28','5','Roxana Elizabeth','Ecuador','Manabi','28','30','2917162','2012-06-14','','Ninguna'),
(4,'1','Urticaria','1310767055','El carmen','Calle 17 Avenida 28','Ampollas','Soltero','1972-01-06','3','Jonathan Javier','Ecuador','Manabi','26','28','2162987','2018-09-30','','Ninguna'),
(5,'1','Moho','1310767065','Pichincha','Calle 104 Avenida 135','Quemaduras','Soltera','1980-10-09','4','Maria Margarita','Ecuador','Manabi','38','40','2987354','2019-10-11','','Ninguna');

/*
Insertar registros a nuestra tabla ASILO
*/
insert into ASILO values
(1,'ASILO NUEVA VIDA','CALLE 13 AVENIDA 24','nuevavida@gmail.com','0989250659','2629838','wwww.nuevavida.com');

/*
Insertar registros a nuestra tabla BODEGA
*/
insert into BODEGA values
(1,'1','001','Bodega entrada','Herramientas de trabajo'),
(2,'1','002','Bodega centro','Productos');


/*
Insertar registros a nuestra tabla EMPLEADO
*/
insert into EMPLEADO values
(1,'1','Director','1306169903','davidvelez@gmail.com','Calle 25 Avenida 120','1990-03-17','0945463336','Eugenio David Velez Mendoza','Ecuador','Manabi','2543789','2018-03-25','','Ninguna'),
(2,'1','Secretario','1306169913','Ginaloor@gmail.com','Calle 32 Avenida 16','1985-07-23','0952562564','Carlos Javier Cedeño Delgado','Ecuador','Manabi','2324984','2019-07-04','','Ninguna'),
(3,'1','Enfermero','1306169923','Juansefas@gmail.com','Calle 87 Avenida 76','1997-01-10','0964646646','Sergio Fabricio Arteaga','Ecuador','Manabi','2653565','2015-11-10','','Ninguna'),
(4,'1','Bodeguero','1306169934','javierzzz@gmail.com','Calle 21 Avenida 43','1982-10-19','0986787698','Diego Javier Piguave Vasquez','Ecuador','Manabi','2876566','2013-03-17','','Ninguna'),
(5,'1','Limpieza','1306169954','jacinto@gmail.com','Call2 11 Avenida 67','1980-12-27','0983635252','Xavier Alexander Alcivar','Ecuador','Manabi','2654678','2020-02-12','','Ninguna'),
(6,'1','Conserje','1306169947','sergio1997@gmail.com','Calle 2 Avenida 23','1974-09-22','0956564545','Erick Joel Espinal Loor','Ecuador','Manabi','2987533','2017-08-16','','Ninguna'),
(7,'1','Enfermera','1306169918','maribella@gmail.com','Calle 44 Avenida 98','1977-05-16','0965656526','Eugenio David Velez Mendoza','Ecuador','Manabi','2345664','2018-09-07','','Ninguna');

/*
Insertar registros a nuestra tabla MENU_COMIDA
*/
insert into MENU_COMIDA values
(1,'1','Arroz verde con langostinos','Domingo'),
(2,'1','Canelones rellenos de calabaza y queso','Lunes'),
(3,'1','Tortitas de pavo y champiñones','Martes'),
(4,'1','Pimientos rellenos con crema de tortilla','Miercoles'),
(5,'1','Hamburguesa de salmón con ensaladilla de patata','Jueves'),
(6,'1','Filetes de pollo rellenos de jamón y queso','Viernes'),
(7,'1','Flan de leche de oveja','Sabado');

/*
Insertar registros a nuestra tabla PRODUCTO
*/
insert into PRODUCTO values
(1,'1','1','Mantenimiento','32143','Escobas','2020-05-12','2020-07-12','Sampedrina','20','Bodega entrada','Disponible'),
(2,'1','2','Higiene','32123','Champú','2020-05-12','2020-07-12','SuperStar','30','Bodega centro','Disponible'),
(3,'1','1','Mantenimiento','32144','Palas de basura','2020-05-12','2020-07-12','Sampedrina','20','Bodega entrada','Disponible'),
(4,'1','2','Higiene','32123','Colonia','2020-05-12','2020-07-12','Yanbal','10','Bodega centro','Disponible'),
(5,'1','1','Mantenimiento','32145','Fundas de basura','2020-05-12','2020-07-12','Sampedrina','20','Bodega entrada','Disponible'),
(6,'1','2','Higiene','32123','Pañales para adulto','2020-05-12','2020-07-12','SuperStar','100','Bodega centro','Disponible');

