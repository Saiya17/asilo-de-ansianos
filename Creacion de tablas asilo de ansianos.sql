/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     30/11/2020 13:34:22                          */
/*==============================================================*/


drop table if exists ANCIANO;

drop table if exists ASILO;

drop table if exists BODEGA;

drop table if exists EMPLEADO;

drop table if exists MENU_COMIDA;

drop table if exists PRODUCTO;

/*==============================================================*/
/* Table: ANCIANO                                               */
/*==============================================================*/
create table ANCIANO
(
   CODE_ANCIANO         int not null,
   CODE_ASILO           int,
   ALERGIAS             char(30) not null,
   CEDULA               int not null,
   CIUDAD               char(30) not null,
   DIRECCION            varchar(50) not null,
   ENFERMEDADES         varchar(50) not null,
   ESTADO_CIVIL         char(20) not null,
   FECHA_DE_NACIMIENTO  date not null,
   HIJOS                int not null,
   NOMBRE               char(30) not null,
   PAIS                 varchar(20) not null,
   PROVINCIA            varchar(30) not null,
   TALLA_PANTALON       decimal not null,
   TALLA_SACO           decimal not null,
   TELEFONO_FAMILIAR    int not null,
   FECHA_DE_ENTRADA     date not null,
   FECHA_DE_SALIDA      date null,
   OBSERVACIONES        varchar(100) not null,
   primary key (CODE_ANCIANO)
);

/*==============================================================*/
/* Table: ASILO                                                 */
/*==============================================================*/
create table ASILO
(
   CODE_ASILO           int not null,
   NOMBRE               char(30) not null,
   DIRECCION            varchar(50) not null,
   EMAIL                varchar(20) not null,
   CELULAR              int not null,
   TELEFONO             int not null,
   PAGINA_WEB           varchar(30) not null,
   primary key (CODE_ASILO)
);

/*==============================================================*/
/* Table: BODEGA                                                */
/*==============================================================*/
create table BODEGA
(
   CODE_BODEGA          int not null,
   CODE_ASILO           int,
   NUMERO_BODEGA        int not null,
   NOMBRE_BODEGA        varchar(30) not null,
   CATEGORIA            varchar(30) not null,
   primary key (CODE_BODEGA)
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO
(
   CODE_EMPLEADO        int not null,
   CODE_ASILO           int,
   CARGO                varchar(20) not null,
   CEDULA               int not null,
   CORREO               varchar(20) not null,
   DIRECCION            varchar(50) not null,
   FECHA                date not null,
   CELULAR              int not null,
   NOMBRE               char(30) not null,
   PAIS                 varchar(20) not null,
   PROVINCIA            varchar(30) not null,
   TELEFONO             int not null,
   FECHA_DE_ENTRADA     date not null,
   FECHA_DE_SALIDA      date null,
   OBSERVACIONES        varchar(100) not null,
   primary key (CODE_EMPLEADO)
);

/*==============================================================*/
/* Table: MENU_COMIDA                                           */
/*==============================================================*/
create table MENU_COMIDA
(
   CODE_COMIDA          int not null,
   CODE_ASILO           int,
   NOMBRE               char(100) not null,
   DIA                  char(20)not null,
   primary key (CODE_COMIDA)
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO
(
   CODE_PRODUCTO        int not null,
   CODE_ASILO           int,
   BODEGA               int not null,
   CATEGORIA            varchar(30) not null,
   CODIGO               int not null,
   DESCRIPCION          varchar(100) not null,
   FECHA_DE_ALTA        date not null,
   FECHA_DE_ENTREGA     date not null,
   NOMBRE               char(30) not null,
   STOCK                int not null,
   UBICACION            varchar(50) not null,
   ESTADO               varchar(15) not null,
   primary key (CODE_PRODUCTO)
);

alter table ANCIANO add constraint FK_TIENE foreign key (CODE_ASILO)
      references ASILO (CODE_ASILO);

alter table BODEGA add constraint FK_RELATIONSHIP_1 foreign key (CODE_ASILO)
      references ASILO (CODE_ASILO);

alter table EMPLEADO add constraint FK_CONTIENE foreign key (CODE_ASILO)
      references ASILO (CODE_ASILO);

alter table MENU_COMIDA add constraint FK_ALMACENA foreign key (CODE_ASILO)
      references ASILO (CODE_ASILO);

alter table PRODUCTO add constraint FK_PUEDE foreign key (CODE_ASILO)
      references ASILO (CODE_ASILO);

